package com.example.adilla_1202160089_si4001_pab_modul1v2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText Alas;
    private EditText Tinggi;
    private TextView Hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void hitung(View view){
        Alas = findViewById(R.id.alas);
        Tinggi =  findViewById(R.id.tinggi);
        Hasil = findViewById(R.id.textView2);

        Integer alas = Integer.parseInt(Alas.getText().toString());
        Integer tinggi = Integer.parseInt(Tinggi.getText().toString());
        Integer hasil = alas*tinggi;
        Hasil.setText(String.valueOf(hasil));
    }
}
